package com.ybg.report.rtemplate.dao;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;
import com.ybg.base.jdbc.BaseDao;
import com.ybg.base.jdbc.BaseMap;
import com.ybg.base.jdbc.util.QvoConditionUtil;
import com.ybg.base.util.Page;
import com.ybg.report.rtemplate.domain.RtemplateVO;
import com.ybg.report.rtemplate.qvo.RtemplateQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import com.ybg.base.jdbc.DataBaseConstant;
import org.springframework.jdbc.core.BeanPropertyRowMapper;

/** @author Deament
 * @email 591518884@qq.com
 * @date 2018-02-21 */
@Repository
public class RtemplateDaoImpl extends BaseDao implements RtemplateDao {
	
	@Autowired
	/** 注入你需要的数据库 **/
	@Qualifier(DataBaseConstant.JD_REPORT)
	JdbcTemplate jdbcTemplate;
	
	@Override
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}
	
	private static String	QUERY_TABLE_COLUMN	= "  rtemplate.filename, rtemplate.filecontent,rtemplate.gmt_modified,rtemplate.gmt_create, rtemplate.id";
	private static String	QUERY_TABLE_NAME	= "report_rtemplate  rtemplate";
	
	@Override
	public RtemplateVO save(RtemplateVO rtemplate) throws Exception {
		BaseMap<String, Object> createmap = new BaseMap<String, Object>();
		String id = null;
		createmap.put("filename", rtemplate.getFilename());
		createmap.put("filecontent", rtemplate.getFilecontent());
		id = baseCreate(createmap, "report_rtemplate", "id");
		rtemplate.setId((String) id);
		return rtemplate;
	}
	
	@Override
	public void update(BaseMap<String, Object> updatemap, BaseMap<String, Object> WHEREmap) {
		this.baseupdate(updatemap, WHEREmap, "report_rtemplate");
	}
	
	@Override
	public Page list(Page page, RtemplateQuery qvo) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append(SELECT).append(QUERY_TABLE_COLUMN).append(FROM).append(QUERY_TABLE_NAME);
		sql.append(getcondition(qvo));
		page.setTotals(queryForInt(sql));
		if (page.getTotals() > 0) {
			page.setResult(getJdbcTemplate().query(page.getPagesql(sql), new BeanPropertyRowMapper<RtemplateVO>(RtemplateVO.class)));
		}
		else {
			page.setResult(new ArrayList<RtemplateVO>());
		}
		return page;
	}
	
	private String getcondition(RtemplateQuery qvo) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append(WHERE).append("1=1");
		sqlappen(sql, "rtemplate.id", qvo.getId());
		if (QvoConditionUtil.checkString(qvo.getFilename())) {
			sql.append(AND).append("rtemplate.filename='").append(qvo.getFilename()).append("'");
		}
		return sql.toString();
	}
	
	@Override
	public List<RtemplateVO> list(RtemplateQuery qvo) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append(SELECT).append(QUERY_TABLE_COLUMN).append(FROM).append(QUERY_TABLE_NAME);
		sql.append(getcondition(qvo));
		return getJdbcTemplate().query(sql.toString(), new BeanPropertyRowMapper<RtemplateVO>(RtemplateVO.class));
	}
	
	@Override
	public void remove(BaseMap<String, Object> wheremap) {
		baseremove(wheremap, "report_rtemplate");
	}
	
	@Override
	public RtemplateVO get(String id) {
		StringBuilder sql = new StringBuilder();
		sql.append(SELECT).append(QUERY_TABLE_COLUMN).append(FROM).append(QUERY_TABLE_NAME);
		sql.append(WHERE).append("1=1");
		sql.append(AND).append("id='" + id + "'");
		List<RtemplateVO> list = getJdbcTemplate().query(sql.toString(), new BeanPropertyRowMapper<RtemplateVO>(RtemplateVO.class));
		return QvoConditionUtil.checkList(list) ? list.get(0) : null;
	}
}
