package com.ybg.report.datatable.service;
import java.util.List;
import com.ybg.report.datatable.domain.DataTableDO;
import com.ybg.report.rtable.domain.RtableVO;

/** @author Deament */
public interface DataTableService {
	
	/** 先删除某个部门的 然后再插入数据（推荐）
	 * 
	 * @param list
	 *            部门数据
	 * @param departmentno
	 *            部门编码 */
	void saveDateRemoveOld(List<DataTableDO> list, String departmentno, String dataTableName);
	
	/** 删除某个部门的数据 **/
	void removeDepartmentData(String departmentno, String dataTableName);
	
	/** 数据库数据转换成HandsonTable数据 **/
	String[][] converToHansonTable(List<DataTableDO> list);
	
	/** HandsonTable数据 转换成 数据库数据 ***/
	List<DataTableDO> converToDataBase(String[][] data,String departmentno,RtableVO table);
}
