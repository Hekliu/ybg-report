package com.ybg.report.datatable.service;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.ybg.report.datatable.dao.DataTableDao;
import com.ybg.report.datatable.domain.DataTableDO;
import com.ybg.report.rtable.domain.RtableVO;
import net.sf.json.JSONObject;

@Repository
public class DataTableServiceImpl implements DataTableService {
	
	@Autowired
	DataTableDao dataTableDao;
	
	@Override
	public void saveDateRemoveOld(List<DataTableDO> list, String departmentno, String dataTableName) {
		dataTableDao.saveDateRemoveOld(list, departmentno, dataTableName);
	}
	
	@Override
	public void removeDepartmentData(String departmentno, String dataTableName) {
		dataTableDao.removeDepartmentData(departmentno, dataTableName);
	}
	
	@Override
	public String[][] converToHansonTable(List<DataTableDO> list) {
		return null;
	}
	
	@Override
	public List<DataTableDO> converToDataBase(String[][] data, String departmentno, RtableVO table) {
		String dataArea = table.getDataarea();
		int startrow = 0;
		int endrow = 0;
		int startcolumn = 0;
		int endcolumn = 0;
		List<DataTableDO> list= new ArrayList<>();
		return list;
	}
}
