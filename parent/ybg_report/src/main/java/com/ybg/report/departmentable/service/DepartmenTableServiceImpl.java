package com.ybg.report.departmentable.service;
import org.springframework.stereotype.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import com.ybg.report.departmentable.dao.DepartmenTableDao;

import com.ybg.report.departmentable.domain.DepartmenTableVO;
import com.ybg.report.departmentable.qvo.DepartmenTableQuery;
import java.util.List;

import com.ybg.base.jdbc.BaseMap;
import com.ybg.base.util.Page;

/**
 * 
 * 
 * @author Deament
 * @email 591518884@qq.com
 * @date 2018-02-21
 */

@Repository
public class DepartmenTableServiceImpl implements DepartmenTableService {
	@Autowired
	private DepartmenTableDao departmenTableDao;
	
	@Override	
	public DepartmenTableVO save(DepartmenTableVO bean) throws Exception{
	return departmenTableDao.save(bean);
	
	}
	
	@Override
	public	void update(BaseMap<String, Object> updatemap, BaseMap<String, Object> wheremap){
		departmenTableDao.update(updatemap,wheremap);
	}
	
	
	@Override
	public	Page list(Page page,DepartmenTableQuery qvo)throws Exception{
		return departmenTableDao.list(page,qvo);
	}
	
	@Override
	public	List<DepartmenTableVO> list(DepartmenTableQuery qvo) throws Exception{
		return departmenTableDao.list(qvo);
	}
   @Override
	public void remove(BaseMap<String, Object> wheremap){
		departmenTableDao.remove(wheremap);
	}
   @Override
	public DepartmenTableVO get(String id){
		return 	departmenTableDao.get(id);
	}
}
